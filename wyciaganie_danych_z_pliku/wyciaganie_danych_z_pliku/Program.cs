﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace ConsoleApp19
{
    internal class Program
    {
        private static void TypeWrite(string message, int delay = 50)
        {
            for (var i = 0; i < message.Length; i++)
            {
                Console.Write(message[i]);
                Thread.Sleep(delay);
            }

            Console.WriteLine();
        }

        private static void Main(string[] args)
        {
            TypeWrite("Hello World");

            // Wykorzystanie regular expression
            Regex annaCheckRule = new(@"^Anna");
            string filePath = @"Lista imion i nazwisk.txt";

            if (!File.Exists(filePath))
            {
                TypeWrite("File read error");
            }
            else
            {
                TypeWrite("File loaded . . .", 100);
                var lineList = new List<string>(File.ReadAllLines(filePath));
                uint counter = 1;
                string selectedItem = "";
                Console.WriteLine("------");
                foreach (var item in lineList)
                {
                    if (counter == 246)
                        selectedItem = item;
                    if (annaCheckRule.IsMatch(item))
                        Console.WriteLine(item);
                    counter++;
                }
                Console.WriteLine("------");
                TypeWrite($"Element 246 to: {selectedItem}");
            }

            Console.ReadLine();
        }
    }
}