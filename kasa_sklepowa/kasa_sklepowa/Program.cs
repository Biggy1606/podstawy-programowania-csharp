﻿using System;
using System.Threading;

// Za pomocą instrukcji warunkowych, pętli oraz typów strukturowych napisać program który obsługuje prostą kasę sklepową z użyciem metod.
//     1. Sklep ma mieć zdefiniowaną strukturę artykułów.
//        - Artykułów ma być max 5. zdefiniowanych jako elementy struktury!
//     2.Należy uwzględnić cenę za sztukę i osobno cenę za kg!
//
// Użytkownik (Kasjer) ma mieć możliwość wybierania artykułu, podawania jego ilości, lub wagi.
//
// # Działanie programu:
// Powitanie ✅
// Wybór artykułu ✅
// Czyszczenie ekranu i zadanie pytania o ilość danego artykułu ✅
// Wprowadzenie danych ✅
// Pytanie czy kasjer chce dodać następny czy ma wydrukować paragon ✅
// Jeśli następny towar, ponowne czyszczenie ekranu i wybór artykułu ✅
// powtórzenie dodawania i pytania ✅
// Jeśli zakończenie to ma pojawić się lista zakupionych towarów wraz z ich ilością oraz ceną, oraz podsumowanie kwoty całego rachunku
// Opcja zamknij program / Nowy Klient
internal class Program
{
    public static void Main(string[] args)
    {
        // Utworzenie struktury 'articles' i dodanie do niej produktów
        var articles = new Items();
        articles.milk = new Item("Łaciate 3.2%", 2.65, false, 1);
        articles.onion = new Item("Polska cebula", 3.99, true, 1);
        articles.fish = new Item("Łosoś", 49.90, true, 1);
        articles.potato = new Item("Polski ziemniak", 2.60, true, 1);
        articles.candy = new Item("Mieszanka krakowska", 17.90, true, 1);
        // Przygotowanie zmiennej z listą zakupionych przedmiotów
        Item[] bill = new Item[50];
        var articlesLoop = true;
        var menuLoop = true;
        uint billIndex = 0;
        double billSum = 0;

        void TypeWriterEffect(string text, int delay = 50, bool center = false, bool alignRight = false)
        {
            if (center) Console.SetCursorPosition((Console.WindowWidth - text.Length) / 2, Console.CursorTop);
            if (alignRight) Console.CursorLeft = Console.BufferWidth - text.Length;
            for (var i = 0; i < text.Length; i++)
            {
                Console.Write(text[i]);
                Thread.Sleep(delay);
            }

            Console.WriteLine();
        }

        void Welcome()
        {
            Console.Clear();
            TypeWriterEffect("Kasa is booting . . .", 250);
            Console.Clear();
            TypeWriterEffect("Witaj xyz!", center: true);
            TypeWriterEffect("Zajmij sie klientami i daj nam zarobić.", center: true);
            TypeWriterEffect("Powodzenia :)", center: true);
            TypeWriterEffect("~Szef");
        }

        bool NextClient()
        {
            string select;
            TypeWriterEffect("Następny klient? [y/N]", 20);
            try
            {
                select = Console.ReadLine();
                if (select[0] == 'y' || select[0] == 'Y')
                {
                    billSum = 0;
                    billIndex = 0;
                    articlesLoop = true;
                    Array.Clear(bill, 0, bill.Length);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        void ShowAvaliableArticles(int delay = 20)
        {
            TypeWriterEffect($"[1] {articles.milk.name}", delay);
            TypeWriterEffect($"[2] {articles.onion.name}", delay);
            TypeWriterEffect($"[3] {articles.fish.name}", delay);
            TypeWriterEffect($"[4] {articles.potato.name}", delay);
            TypeWriterEffect($"[5] {articles.candy.name}", delay);
        }

        void DebugItem(Item item)
        {
            TypeWriterEffect($"{item.ammount}\n{item.name}\n{item.price}\n{item.weightable}", 20);
            Console.ReadLine();
        }

        void AddArticleToBill(Item item, ref uint billIndex)
        {
            string isKg = item.weightable ? "kg" : "szt";
            TypeWriterEffect($"Podaj ilość [{isKg}]:", 20);
            double userAmmount;
            userAmmount = double.Parse(Console.ReadLine());
            item.ammount = userAmmount;
            bill[billIndex] = item;
            billIndex++;
        }

        void SelectArticle(ref uint billIndex)
        {
            uint selection = 0;
            TypeWriterEffect("Wybierz artykuł: ", 20);
            ShowAvaliableArticles(15);
            try
            {
                selection = uint.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                selection = 0;
            }

            Console.Clear();
            switch (selection)
            {
                case 1:
                    AddArticleToBill(articles.milk, ref billIndex);
                    break;
                case 2:
                    AddArticleToBill(articles.onion, ref billIndex);
                    break;
                case 3:
                    AddArticleToBill(articles.fish, ref billIndex);
                    break;
                case 4:
                    AddArticleToBill(articles.potato, ref billIndex);
                    break;
                case 5:
                    AddArticleToBill(articles.candy, ref billIndex);
                    break;
            }
        }

        bool PrintBill()
        {
            string select;
            TypeWriterEffect("Wydrukować paragon? [y/N]", 20);
            try
            {
                select = Console.ReadLine();
                if (select[0] == 'y' || select[0] == 'Y')
                {
                    Console.Clear();
                    for (var i = 0; i < billIndex; i++)
                    {
                        billSum += bill[i].price * bill[i].ammount;
                        string isKg = bill[i].weightable ? "kg" : "szt";
                        TypeWriterEffect(
                            $"{bill[i].name} - {bill[i].ammount.ToString("#.##")}{isKg} {(bill[i].price * bill[i].ammount).ToString("#.##")}zł",
                            alignRight: true, delay: 20);
                    }

                    TypeWriterEffect($"Suma: {billSum.ToString("#.##")}zł", 20);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        void ShowBillCount(ref Item[] array)
        {
            TypeWriterEffect($"Produktów na rachunku: {billIndex} / {array.Length}", alignRight: true, delay: 20);
        }

        /*
         *  Main
         */
        Welcome();
        Thread.Sleep(1000);
        Console.Clear();
        while (menuLoop)
        {
            while (articlesLoop)
            {
                Console.Clear();
                ShowBillCount(ref bill);
                SelectArticle(ref billIndex);
                Thread.Sleep(500);
                Console.Clear();
                articlesLoop = !PrintBill();
            }

            menuLoop = NextClient();
            Console.Clear();
        }
    }

    public struct Item
    {
        public double price; // cena za sztuke / za kilo
        public bool weightable; // czy przedmiot wystepuje na wage
        public string name; // nazwa produktu
        public double ammount; // ilosc sztuk / kilogramow

        public Item(string name, double price, bool weightable, int ammount)
        {
            this.name = name;
            this.price = price;
            this.weightable = weightable;
            this.ammount = ammount;
        }
    }

    public struct Items
    {
        public Item milk; //na sztuki
        public Item onion; //na wage
        public Item potato; //na wage
        public Item fish; //na wage
        public Item candy; //na wage
    }
}