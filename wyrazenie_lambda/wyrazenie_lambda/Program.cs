﻿// See https://aka.ms/new-console-template for more information

using System.Linq.Expressions;

var a = 10;
Expression<Func<int>> sum = () => 1 + a + 3 + 4;
var sumCompiled = sum.Compile();
Console.WriteLine(sumCompiled());
