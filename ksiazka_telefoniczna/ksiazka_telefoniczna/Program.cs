﻿using System;
using System.Collections.Generic;
using System.Linq;

// Napisać prostą książkę telefoniczną z poniższymi funkcjami wywoływanymi spod komend klawiatury:
// - [w] Wyświetlanie całej książki telefonicznej
// - [s] Szukanie po nazwisku i imieniu
// - [a] Szukanie po numerze telefonu
// - [d] Dodawanie nowego wpisu do książki
//
//     Książka początkowo ma posiadać 10 wpisów.
//
//     Po każdym wywołaniu opcji wcześniejsze wyświetlenie danych ma być czyszczone. Program ma działać w pętli do.. while

namespace SortedDictionary
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SortedDictionary<string, string> definition = new();
            definition.Add("Maria Maciejczyk", "300-200-100");
            definition.Add("Halina Maciejczyk", "000-200-000");
            definition.Add("Anastazja Krup", "302-300-300");
            definition.Add("Emilia Górecka", "300-333-300");
            definition.Add("Olga Kozłowska", "300-444-300");
            definition.Add("Marcela Piotrowska", "777-300-300");
            definition.Add("Amelia Duda", "300-300-970");
            definition.Add("Marlena Dąbrowska", "960-350-300");
            definition.Add("Martyna Kowalczyk", "300-500-300");
            definition.Add("Julita Pawlak", "300-300-640");
            do
            {
                Console.WriteLine("Wybierz opcję: \n");
                Console.WriteLine(
                    "[w] Wyświetlanie całej książki telefonicznej\n[s] Szukanie po nazwisku i imieniu\n[a] Szukanie po numerze telefonu\n[d] Dodawanie nowego wpisu do książki");
                var keyInfo = Console.ReadKey();

                if (keyInfo.Key == ConsoleKey.W)
                {
                    Console.Clear();
                    Console.WriteLine("-------------------------");
                    Console.ForegroundColor = ConsoleColor.White;
                    foreach (var def in definition) Console.WriteLine($"{def.Key}: {def.Value}");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("-------------------------");
                }
                else if (keyInfo.Key == ConsoleKey.S)
                {
                    Console.Clear();
                    Console.Clear();
                    Console.WriteLine("Podaj imię i nazwisko którego szukasz: ");
                    Console.WriteLine("-------------------------");
                    string name = (string) Console.ReadLine();
                    try
                    {
                        Console.WriteLine($"{name}: {definition[name]}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Nie znaleziono numeru dla tej osoby.");
                    }
                }
                else if (keyInfo.Key == ConsoleKey.A)
                {
                    Console.Clear();
                    Console.WriteLine("Podaj numer którego szukasz: ");
                    string number = (string) Console.ReadLine();
                    try
                    {
                        var item = definition.First(item => item.Value == number);
                        Console.WriteLine($"{item.Key}: {item.Value}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Nie znaleziono takiego numeru");
                    }

                    Console.WriteLine("-------------------------");
                }
                else if (keyInfo.Key == ConsoleKey.D)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Wprowadź imię i nazwisko: ");
                    string name = Console.ReadLine();

                    Console.WriteLine("Wprowadz numer telefonu: ");
                    string number = Console.ReadLine();

                    definition[name] = number;
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                else
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Błędna opcja.\nCzy chcesz wyjść z programu? \n Naciśnij [t] - tak, [n]- nie");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    if (Console.ReadKey().Key == ConsoleKey.T) break;
                }
            } while (true);
        }
    }
}